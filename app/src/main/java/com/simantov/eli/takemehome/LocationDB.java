package com.simantov.eli.takemehome;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by esimanto on 5/11/2015.
 */
public class LocationDB {
    private int _id;
    private double lat;
    private double lng;
    private String zom;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getZom() {
        return zom;
    }

    public void setZom(String zom) {
        this.zom = zom;
    }
}
