package com.simantov.eli.takemehome;

import android.app.Instrumentation;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
//import android.location.LocationListener;
import android.app.AlertDialog;
import android.preference.DialogPreference;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.content.ContentResolver;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TakeMeHomeActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LocationManager locationMangaer=null;
    private static final String TAG = "TakeMeHomeApp";
    ArrayList<LatLng> markerPoints;
    TextView tvDistanceDuration;
    private Switch TrafficSwitch;


    private final LatLng LOCATION_VIRGINIA_BEACH = new LatLng(36.8506, -75.9779);
    public final LatLng LOCATION_HOME2 = new LatLng(38.796774, -76.132595);
    private LatLng current_coordinates = LOCATION_HOME2;
    public boolean gps_flag = false;
    public boolean check_next_location_provider = false;
    public boolean initial_gps_state_flag = false;
    public boolean wait_flag = true;
    Marker home_marker;
//    public LatLng LOCATION_HOME = new LatLng(36.796774, -76.132595);
    public LatLng LOCATION_HOME = new LatLng(38.796774, -76.132595);
    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;

    private void getLocation(){
        Log.i(TAG, "getLocation Called");
        Location location = locationMangaer.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location!=null) {
            if((System.currentTimeMillis()-(location.getTime()))> 300000) {

                Toast.makeText(getApplicationContext(), "Last location gotten from GPS is too far in the past",
                        Toast.LENGTH_LONG).show();
                check_next_location_provider = true;
            }
             if (true){
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date(location.getTime());
                String formatted = format.format(date);
                LatLng loc_lat_long = new LatLng(location.getLatitude(), location.getLongitude());
                current_coordinates = loc_lat_long;
                Toast.makeText(getBaseContext(), "Location retrieved from GPS location service \n " +
                                "latitude = " + location.getLatitude() + "\n longitude =  " + location.getLongitude()
                                + "\n at " + formatted,
                        Toast.LENGTH_LONG).show();
                check_next_location_provider = false;
            }
        }
        else check_next_location_provider = true;
        if(check_next_location_provider){
            location = locationMangaer.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location!=null) {
                if((System.currentTimeMillis()-(location.getTime()))> 300000) {

                    Toast.makeText(getApplicationContext(), "Last location gotten from Network is too far in the past",
                            Toast.LENGTH_LONG).show();
                    check_next_location_provider = true;
                }
                 if (true){
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date(location.getTime());
                    String formatted = format.format(date);
                    LatLng loc_lat_long = new LatLng(location.getLatitude(), location.getLongitude());
                    current_coordinates = loc_lat_long;
                    Toast.makeText(getBaseContext(), "Location retrieved from Network location service \n " +
                                    "latitude = " + location.getLatitude() + "\n longitude =  " + location.getLongitude()
                                    + "\n at " + formatted,
                            Toast.LENGTH_LONG).show();
                    check_next_location_provider = false;
                }
            }
            else check_next_location_provider = true;
            if(check_next_location_provider){
                location = locationMangaer.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                if (location!=null) {
                    if((System.currentTimeMillis()-(location.getTime()))> 300000) {

                        Toast.makeText(getApplicationContext(), "Last location gotten from Passive is too far in the past",
                                Toast.LENGTH_LONG).show();
                        check_next_location_provider = true;
                    }
                     if (true){
                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = new Date(location.getTime());
                        String formatted = format.format(date);
                        LatLng loc_lat_long = new LatLng(location.getLatitude(), location.getLongitude());
                        current_coordinates = loc_lat_long;
                        Toast.makeText(getBaseContext(), "Location retrieved from Passive location service \n " +
                                        "latitude = " + location.getLatitude() + "\n longitude =  " + location.getLongitude()
                                        + "\n at " + formatted,
                                Toast.LENGTH_LONG).show();
                        check_next_location_provider = false;
                    }
                }
                else check_next_location_provider = true;
            }
            if((check_next_location_provider || location==null)){
                Toast.makeText(getApplicationContext(), "Can not get GPS, Network, or passive location!!! =)",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    public void drawLineToHome(LatLng origin_latlng){
        Log.i(TAG, "drawLineToHome Called");
        PolylineOptions options = new PolylineOptions()
                .add(origin_latlng)
                .add(LOCATION_HOME);
        mMap.addPolyline(options);
        mMap.addMarker(new MarkerOptions().position(LOCATION_HOME).title("You need to get here"));
}

    private String getDirectionsUrl(LatLng origin,LatLng dest){
        Log.i(TAG, "getDirectionsUrl Called");

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;


        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        Log.i(TAG, "downloadUrl Called");
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception in url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected Called");
        Log.i(TAG, "Location services connected.");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
        }
        else {
            Log.i(TAG, "Using last location");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        Log.i(TAG, "handleNewLocation Called");
        Log.d(TAG, location.toString());
        LatLng loc_lat_long = new LatLng(location.getLatitude(), location.getLongitude());
        current_coordinates = loc_lat_long;
        setUpMap(loc_lat_long);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended Called");
        Log.i(TAG, "Location services suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed Called");
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged Called");
        handleNewLocation(location);
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            Log.i(TAG, "onPostExecute Called");
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

try{
            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);
                    if(j==0){	// Get distance from the list
                        distance = (String)point.get("distance");
//                        Toast.makeText(getApplicationContext(), "Getting distance " + distance,
//                                Toast.LENGTH_LONG).show();
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }


                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.MAGENTA);


            }
            tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }catch(Exception e){
            e.printStackTrace();

        }

    }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult Called");
        super.onActivityResult(requestCode, resultCode, data);
        wait_flag = false;
//        getLocation();
//        setUpMap(current_coordinates);
    }
    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy Called");
        if (initial_gps_state_flag == false) {
            Toast.makeText(getApplicationContext(), "Would you like to turn off GPS to save power?",
                    Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate Called");
        setContentView(R.layout.activity_take_me_home);
        tvDistanceDuration = (TextView) findViewById(R.id.tvDistanceDuration);

        TrafficSwitch = (Switch) findViewById(R.id.trafficSwitch);
        //attach a listener to check for changes in state
        TrafficSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                     @Override
                                                     public void onCheckedChanged(CompoundButton buttonView,
                                                                                  boolean isChecked) {
                                                         mMap.setTrafficEnabled(isChecked);
                                                     }
                                                 });
         setUpMapIfNeeded();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
         locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//// Define a listener that responds to location updates
//        LocationListener locationListener = new LocationListener() {
//            public void onLocationChanged(Location location) {
//                // Called when a new location is found by the network location provider.
//                LatLng loc_lat_long = new LatLng(location.getLatitude(), location.getLongitude());
//                current_coordinates = loc_lat_long;
//                setUpMap(loc_lat_long);
//
//            }
//            public void onStatusChanged(String provider, int status, Bundle extras) {}
//
//            public void onProviderEnabled(String provider) {
////                alertbox("GPS Status","GPS On");
//            }
//
//            public void onProviderDisabled(String provider) {}
//        };
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000)
                .setSmallestDisplacement(1);   // in meters


//        locationMangaer.requestLocationUpdates(LocationManager
//                .GPS_PROVIDER, 5000, 10, locationListener);
//        if (!hasGPSDevice(this)){
//            Toast.makeText(getApplicationContext(), "This app needs GPS, device appears not to have one!!! =)",
//                    Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "Exiting application!!! =)",
//                    Toast.LENGTH_LONG).show();
//            finish();
//        }
//        else {
//            Toast.makeText(getApplicationContext(), "This app needs GPS, device appears have one!!! =)",
//                    Toast.LENGTH_LONG).show();
//        }

        if(mMap!=null){

//  For Testing with the emulator to get good screen shots
//            String url = getDirectionsUrl(LOCATION_HOME, LOCATION_HOME2);
//            DownloadTask downloadTask = new DownloadTask();
//            downloadTask.execute(url);
//            mMap.setTrafficEnabled(true);
//            setUpMap(LOCATION_HOME);
//            CameraUpdate update;
//            update = CameraUpdateFactory.newLatLngZoom(LOCATION_HOME, 7);
//            mMap.animateCamera(update);

            gps_flag = displayGpsStatus();
            if(gps_flag){
                initial_gps_state_flag = true;
//                getLocation();
            }
            else {
                Toast.makeText(getApplicationContext(), "This app needs location services, please turn it on!!! =)",
                        Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(
                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        myIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//                        myIntent.setComponent(toLaunch);
//                        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(myIntent, 1);

//                Thread thread = new Thread()
//                {
//                    @Override
//                    public void run() {
//                        IntentFilter intF = new IntentFilter(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        Log.i(TAG, "out of intent1");
//
//                        Instrumentation instrumentation = new Instrumentation();
//                        Log.i(TAG, "out of intent2");
//
//                        Instrumentation.ActivityMonitor monitor = instrumentation.addMonitor(intF, null, true);
//                        Log.i(TAG, "out of intent3");
//                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        myIntent.addCategory(Intent.CATEGORY_LAUNCHER);
////                        myIntent.setComponent(toLaunch);
//                        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        Log.i(TAG, "out of intent4");
//                        if (myIntent == null) Log.i(TAG, "myIntent is null");
//                        myIntent.setFlags(myIntent.getFlags() | myIntent.FLAG_ACTIVITY_NEW_TASK);
//                        try{
//                            instrumentation.startActivitySync(myIntent);
//                        }catch (Exception e){
//                            Log.i(TAG, "exception in startActivitySync");
//                            e.printStackTrace();
//                        }
//
//                        Log.i(TAG, "out of intent5");
//                    }
//                };

 //               thread.start();
                Log.i(TAG, "out of intent6");
                try {
                while(!gps_flag){
//                    Log.i(TAG, "in the loop");
                            Thread.sleep(500);                 //1000 milliseconds is one second.
                    gps_flag = displayGpsStatus();

                };
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
//                startActivity(myIntent);
            }
            if (!getHomeLocationFromDB()){
                alertbox("Please setup the Home Location", "Do you want to store the home location using your current location?");
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
            }
            else {
                Log.i(TAG, "Using last location");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                handleNewLocation(location);
            }
            // Enable MyLocation Button in the Map
//            mMap.setMyLocationEnabled(true);

            // Setting onclick event listener for the map
//            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//
//                @Override
//                public void onMapClick(LatLng point) {
//
//                    // Already two locations
//                    if(markerPoints.size()>1){
//                        markerPoints.clear();
//                        mMap.clear();
//                    }
//
//                    // Adding new item to the ArrayList
//                    markerPoints.add(point);
//
//                    // Creating MarkerOptions
//                    MarkerOptions options = new MarkerOptions();
//
//                    // Setting the position of the marker
//                    options.position(point);
//
//                    /**
//                     * For the start location, the color of marker is GREEN and
//                     * for the end location, the color of marker is RED.
//                     */
//                    if(markerPoints.size()==1){
//                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                    }else if(markerPoints.size()==2){
//                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                    }
//
//
//                    // Add new marker to the Google Map Android API V2
//                    mMap.addMarker(options);
//
//                    // Checks, whether start and end locations are captured
//                    if(markerPoints.size() >= 2){
//                        LatLng origin = markerPoints.get(0);
//                        LatLng dest = markerPoints.get(1);
//                        // Getting URL to the Google Directions API
//                        String url = getDirectionsUrl(current_coordinates, LOCATION_HOME);
//                        DownloadTask downloadTask = new DownloadTask();
//                        // Start downloading json data from Google Directions API
//                        downloadTask.execute(url);
//                    }
//
//                }
//            });
       }

    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume Called");
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
        wait_flag = false;
}

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause Called");
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */

    public void Update_Map_from_Lat_Long(LatLng new_coordinates) {
        Log.i(TAG, "Update_Map_from_Lat_Long Called");
        setUpMap(new_coordinates);
    }

    private void setUpMapIfNeeded() {
        Log.i(TAG, "setUpMapIfNeeded Called");
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
           }
        }
    }

      /**
       * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        Log.i(TAG, "setUpMap Called");

        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(LOCATION_HOME).title("Phone home"));
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_VIRGINIA_BEACH, 8);
        //   mMap.animateCamera(update);
        update = CameraUpdateFactory.newLatLngZoom(LOCATION_VIRGINIA_BEACH, 8);
    //    Location myLocation = mMap.getMyLocation();
    //    LatLng LOCATION_MY_LOCATION = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        //    update = CameraUpdateFactory.newLatLngZoom(LOCATION_MY_LOCATION ,16);
        mMap.animateCamera(update);

    }
    private void setUpMap(LatLng new_coordinates) {
        Log.i(TAG, "setUpMap - LatLng Called");
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(new_coordinates).title("You are here"));
        mMap.addMarker(new MarkerOptions().position(LOCATION_HOME).title("Home"));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new_coordinates);
        builder.include(LOCATION_HOME);
        LatLngBounds bounds = builder.build();
        CameraUpdate update;
        if ((current_coordinates.latitude == LOCATION_HOME.latitude) && (current_coordinates.longitude == LOCATION_HOME.longitude)){
            update = CameraUpdateFactory.newLatLngZoom(new_coordinates, 15);
        }
        else  {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;
            int width = displaymetrics.widthPixels;
//            Toast.makeText(getBaseContext(), "display height " +
//                            height + " width " + width,
//                    Toast.LENGTH_LONG).show();
            update = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 125);
        }
//        mMap.animateCamera(update);
        mMap.moveCamera(update);
        float zoon = mMap.getCameraPosition().zoom;
        if (zoon > 16) {
            update = CameraUpdateFactory.newLatLngZoom(new_coordinates, 16);
            mMap.animateCamera(update);
        }
        else {
            mMap.animateCamera(update);
        }
        String url = getDirectionsUrl(current_coordinates, LOCATION_HOME);
        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
        mMap.animateCamera(update);

    }
    public void setLocation() {
        Log.i(TAG, "setLocation Called");
        ContentValues contentValues = new ContentValues();

        // Setting latitude in ContentValues
        contentValues.put(LocationDBHandler.FIELD_LAT, current_coordinates.latitude);

        // Setting longitude in ContentValues
        contentValues.put(LocationDBHandler.FIELD_LNG, current_coordinates.longitude);

        // Setting zoom in ContentValues
        contentValues.put(LocationDBHandler.FIELD_ZOOM, mMap.getCameraPosition().zoom);

        // Creating an instance of LocationInsertTask
        LocationDBHandler insertTask = new LocationDBHandler(this);

        // Storing the latitude, longitude and zoom level to SQLite database
        insertTask.del();
        insertTask.insert(contentValues);
        Toast.makeText(getBaseContext(), "Location saved to phone", Toast.LENGTH_SHORT).show();
        LOCATION_HOME = current_coordinates;
        setUpMap(current_coordinates);

//        LatLng offset = new LatLng(current_coordinates.latitude, current_coordinates.longitude + 0.00001);
//        drawLineToHome(current_coordinates);
    }


    public boolean getHomeLocationFromDB() {
        Log.i(TAG, "getHomeLocationFromDB Called");
        LocationDBHandler getTask = new LocationDBHandler(this);
        Cursor c = getTask.getAllLocations();
        if (c == null) {
            Toast.makeText(getBaseContext(), "Cursor is null from query ",
                    Toast.LENGTH_LONG).show();
            c.close();
            return false;
        }
        if (c.getCount() == 0) {
            Toast.makeText(getBaseContext(), "No stored location available, please set your home location!!!",
                    Toast.LENGTH_LONG).show();
            c.close();
            return false;
        }
        else {
            c.moveToFirst();
            Toast.makeText(getBaseContext(), "Location retrieved from the phone storage " +
                            c.getString(1) + " " + c.getDouble(2),
                            Toast.LENGTH_LONG).show();
        }
        LatLng DBlocation = new LatLng(c.getDouble(1), c.getDouble(2));
        LOCATION_HOME = DBlocation;
        c.close();
        return true;
    }

    public void onGetLocationClick(View v) {
        Log.i(TAG, "onGetLocationClick Called");
//             getHomeLocationFromDB();
//         try {
//             getHomeLocationFromDB();
//         }catch (Exception e){
//             e.printStackTrace();
//        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" +
                current_coordinates.latitude + "," + current_coordinates.longitude + "&daddr=" + LOCATION_HOME.latitude + "," + LOCATION_HOME.longitude));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }


public void onAddressClick(View v){
    Log.i(TAG, "onAddressClick Called");
    Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

    try {
        List<Address> addresses = geocoder.getFromLocation(current_coordinates.latitude, current_coordinates.longitude, 1);

        if(addresses != null) {
            Address returnedAddress = addresses.get(0);
            StringBuilder strReturnedAddress = new StringBuilder("You are at the following address:\n");
            for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
            }
            Toast.makeText(getBaseContext(), strReturnedAddress,
                    Toast.LENGTH_LONG).show();
            StringBuilder strJustReturnedAddress = new StringBuilder("");
            for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                strJustReturnedAddress.append(returnedAddress.getAddressLine(i));
            }

            getCoordinatesFromAddress(strJustReturnedAddress.toString());
        }
        else{
            Toast.makeText(getBaseContext(), "No Address returned!",
                    Toast.LENGTH_LONG).show();
        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        Toast.makeText(getBaseContext(), "Can not get address!",
                Toast.LENGTH_LONG).show();
    }

}
    public void onClick(View v) {
        Log.i(TAG, "onClick Called");
        // Creating an instance of ContentValues
        alertbox("Replace Home Location", "Do you want to replace the home location stored on this phone?");
//        setLocation();
    }



    private boolean hasGPSDevice(Context context)
    {
        Log.i(TAG, "hasGPSDevice Called");
        final LocationManager mgr = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        if ( mgr == null ) return false;
        final List<String> providers = mgr.getAllProviders();
        if ( providers == null ) return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }


    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        Log.i(TAG, "displayGpsStatus Called");
//        ContentResolver contentResolver = getBaseContext()
//                .getContentResolver();
//        boolean gpsStatus = Settings.Secure
//                .isLocationProviderEnabled(contentResolver,
//                        LocationManager.GPS_PROVIDER);

        boolean gps_enabled = false;
        try {
            gps_enabled = locationMangaer.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        boolean network_enabled = false;
        try {
            network_enabled = locationMangaer.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        boolean passive_enabled = false;
//        try {
//            passive_enabled = locationMangaer.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
//        } catch (Exception ex) {
//        }


           return gps_enabled || network_enabled || passive_enabled;
    }


    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        Log.i(TAG, "alertbox Called");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Would you like to update your Home location for this phone?")
                .setCancelable(false)
                .setTitle("** Update Home Location **")
                .setPositiveButton("Save using GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
//                                Intent myIntent = new Intent(
//                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                                startActivity(myIntent);
//                                dialog.cancel();
                                setLocation();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private LatLng getCoordinatesFromAddress(String user_address){
        Log.i(TAG, "getCoordinatesFromAddress Called");
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        double latitude=0, longitude=0;
        List<Address> addresses;

    try{addresses = geocoder.getFromLocationName(user_address, 1);
        if(addresses.size() > 0) {
            latitude= addresses.get(0).getLatitude();
            longitude= addresses.get(0).getLongitude();
            Toast.makeText(getBaseContext(), "From the following address: \n "
                            + user_address +  "\nThe coordinates are \n" +
                    "latitude = " + latitude + "\n longitude =  "  + longitude,
                    Toast.LENGTH_LONG).show();
        }
        else  {
            Toast.makeText(getBaseContext(), "No coordinates returned for the following address! \n"
                            + user_address ,
                    Toast.LENGTH_LONG).show();

        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        Toast.makeText(getBaseContext(), "Can not get coordinates from the following address! \n"
                + user_address,
                Toast.LENGTH_LONG).show();
    }
        return new LatLng(latitude, longitude);

    }

}